import React from 'react';
import Chart from 'react-apexcharts';
import ProgressBar from "@ramonak/react-progress-bar";

export default function Output(props) {
    return (
        <>
            { props.series[0].data ? <Chart type="bar" height={500} options={props.options} series={props.series}/> : null }
            <span id="container" style={styles.progress}>
                { props.loading ?  <ProgressBar width="200" bgColor="#3710e3" completed={props.progress}/> : null }
            </span>
        </>
    );
}

const styles = {
    progress: {
        position: 'fixed',
        top: '50%',
        left: '40%'
    }
};