 function onStartYearChange(props, newValue) {
    const startYear = parseInt(newValue);
    props.setStartYear(startYear);
    if (props.endYear < startYear) {
        props.setEndYear(startYear);
    }
}

function onEndYearChange(props, newValue) {
    const endYear = parseInt(newValue);
    props.setEndYear(endYear);
    if (props.startYear > endYear) {
        props.setStartYear(endYear);
    }
}

export { onStartYearChange, onEndYearChange };

