import { onStartYearChange, onEndYearChange } from './helper.js';

export default function Index(props) {
    return (
        <>
            <label htmlFor="startYear">Start Year:</label>
            <input id="startYear" type="number" disabled={props.loading} min={props.minYear} max={props.maxYear} step="1" value={props.startYear} onChange={event => onStartYearChange(props, event.target.value)} style={styles.startYear}/>
            <label htmlFor="endYear">End Year:</label>
            <input id="endYear" type="number" disabled={props.loading} min={props.minYear} max={props.maxYear} step="1" value={props.endYear} onChange={event => onEndYearChange(props, event.target.value)} style={styles.endYear}/>
        </>
    );
}

const styles = {
    startYear: {
        width: '65px',
        marginLeft: '3px',
        marginRight: '20px',
        textAlign: 'center'
    },
    endYear: {
        width: '65px',
        marginLeft: '3px',
        marginRight: '20px',
        textAlign: 'center'
    }
};