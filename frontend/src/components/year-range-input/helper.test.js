import { onStartYearChange, onEndYearChange } from './helper.js';

describe('yearRangeInputHelper', () => {
    describe('onStartYearChange', () => {
        const props = {
            setStartYear: jest.fn(),
            setEndYear: jest.fn()
        };

        beforeAll(() => {
            props.endYear = 2008;
        });

        describe('when start year before end year', () => {
            const startYear = '2007';

            beforeEach(() => {
                onStartYearChange(props, startYear);
            });

            describe('props.setStartYear', () => {
                it('should be called once', () => {
                    expect(props.setStartYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct start year', () => {
                    expect(props.setStartYear).toHaveBeenCalledWith(2007);
                });
            });

            it('props.setEndYear should not be called', () => {
                expect(props.setEndYear).not.toHaveBeenCalled();
            });
        });

        describe('when start year equal to end year', () => {
            const startYear = '2008';

            beforeEach(() => {
                onStartYearChange(props, startYear);
            });

            describe('props.setStartYear', () => {
                it('should be called once', () => {
                    expect(props.setStartYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct start year', () => {
                    expect(props.setStartYear).toHaveBeenCalledWith(2008);
                });
            });

            it('props.setEndYear should not be called', () => {
                expect(props.setEndYear).not.toHaveBeenCalled();
            });
        });

        describe('when start year after end year', () => {
            const startYear = '2009';

            beforeEach(() => {
                onStartYearChange(props, startYear);
            });

            describe('props.setStartYear', () => {
                it('should be called once', () => {
                    expect(props.setStartYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct start year', () => {
                    expect(props.setStartYear).toHaveBeenCalledWith(2009);
                });
            });

            describe('props.setEndYear', () => {
                it('should be called once', () => {
                    expect(props.setEndYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct end year', () => {
                    expect(props.setEndYear).toHaveBeenCalledWith(2009);
                });
            });
        });
    });

    describe('onEndYearChange', () => {
        const props = {
            setStartYear: jest.fn(),
            setEndYear: jest.fn()
        };

        beforeAll(() => {
            props.startYear = 2008;
        });

        describe('when end year before start year', () => {
            const endYear = '2007';

            beforeEach(() => {
                onEndYearChange(props, endYear);
            });

            describe('props.setStartYear', () => {
                it('should be called once', () => {
                    expect(props.setStartYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct start year', () => {
                    expect(props.setStartYear).toHaveBeenCalledWith(2007);
                });
            });

            describe('props.setEndYear', () => {
                it('should be called once', () => {
                    expect(props.setEndYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct end year', () => {
                    expect(props.setEndYear).toHaveBeenCalledWith(2007);
                });
            });
        });

        describe('when end year equal to start year', () => {
            const endYear = '2008';

            beforeEach(() => {
                onEndYearChange(props, endYear);
            });

            it('props.setStartYear should not be called', () => {
                expect(props.setStartYear).not.toHaveBeenCalled();
            });

            describe('props.setEndYear', () => {
                it('should be called once', () => {
                    expect(props.setEndYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct end year', () => {
                    expect(props.setEndYear).toHaveBeenCalledWith(2008);
                });
            });
        });

        describe('when end year after start year', () => {
            const endYear = '2009';

            beforeEach(() => {
                onEndYearChange(props, endYear);
            });

            it('props.setStartYear should not be called', () => {
                expect(props.setStartYear).not.toHaveBeenCalled();
            });

            describe('props.setEndYear', () => {
                it('should be called once', () => {
                    expect(props.setEndYear).toHaveBeenCalledTimes(1);
                });

                it('should be called with correct end year', () => {
                    expect(props.setEndYear).toHaveBeenCalledWith(2009);
                });
            });
        });
    });
});
