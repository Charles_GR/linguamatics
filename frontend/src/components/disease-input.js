export default function DiseaseInput(props) {
    return (
        <>
            <label htmlFor="disease">Disease/Disease Area:</label>
            <input id="disease" type="text" name="disease" disabled={props.loading} style={styles.disease} value={props.disease} onChange={event => props.setDisease(event.target.value)}/>
        </>
    );
};

const styles = {
    disease: {
        width: '100px',
        marginLeft: '3px',
        marginRight: '20px'
    }
};

