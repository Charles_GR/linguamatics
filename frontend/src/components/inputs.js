import React from 'react';
import DiseaseInput from './disease-input';
import YearRangeInput from './year-range-input';

export default function Inputs(props) {
    return (
        <span id="inputs" style={styles.inputs}>
            <DiseaseInput
                disease={props.disease}
                setDisease={props.setDisease}
                loading={props.loading}/>
            <YearRangeInput
                minYear={1970}
                maxYear={2021}
                startYear={props.startYear}
                setStartYear={props.setStartYear}
                endYear={props.endYear}
                setEndYear={props.setEndYear}
                loading={props.loading}/>
            <button id="drawChart" disabled={props.loading} onClick={props.drawChart} style={styles.drawChart}>Draw Chart</button>
        </span>
    );
}

const styles = {
    inputs: {
        textAlign: 'center',
        display: 'block'
    },
    drawChart: {
        marginLeft: '20px'
    }
};