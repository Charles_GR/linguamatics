const initialChartOptions = {
    dataLabels: {
        enabled: false
    },
    xaxis: {}
};

const initialChartSeries = {
    name: 'Articles published'
};

async function drawChart(disease, startYear, endYear, setChartSeries, setChartOptions, setProgress, setLoading) {
    setChartSeries([ initialChartSeries ]);
    setProgress(0);
    setLoading(true);

    const data = await fetchData(disease, startYear, endYear, setProgress);
    setNewChartOptions(data, setChartOptions);
    setNewChartSeries(data, setChartSeries);
    setLoading(false);
}

async function fetchData(disease, startYear, endYear, setProgress) {
    const data = new Map();
    for (let year = startYear; year <= endYear; year++) {
        const response = await fetch(`http://localhost:8000/count?disease=${disease}&year=${year}`);
        if (response.ok) {
            let result = await response.json();
            data.set(year, result.count);
            setProgress(Math.round((year - startYear + 1) / (endYear - startYear + 1) * 100));
        }
    }
    return data;
}

function setNewChartOptions(data, setChartOptions) {
    const newOptions = Object.assign({}, initialChartOptions);
    newOptions.xaxis.categories = Array.from(data.keys());
    setChartOptions(newOptions);
}

function setNewChartSeries(data, setChartSeries) {
    const newSeries = [ Object.assign({}, initialChartSeries) ];
    newSeries[0].data = Array.from(data.values());
    setChartSeries(newSeries);
}

export { initialChartOptions, initialChartSeries, drawChart };