import { initialChartSeries, drawChart } from './helper.js';

describe('appHelper', () => {
    describe('drawChart', () => {
        const disease = 'cancer';
        const startYear = 2010;
        const endYear = 2011;
        const setChartSeries = jest.fn();
        const setChartOptions = jest.fn();
        const setProgress = jest.fn();
        const setLoading = jest.fn();
        let fetch;

        beforeEach(async () => {
            fetch = jest.spyOn(global, 'fetch').mockResolvedValue({
                ok: true,
                json: jest.fn().mockResolvedValue({ count: 5 })
            })
            await drawChart(disease, startYear, endYear, setChartSeries, setChartOptions, setProgress, setLoading);
        });

        describe('fetch', () => {
            it('should be called twice', () => {
                expect(fetch).toHaveBeenCalledTimes(2);
            });

            it('should be called with correct url first time', () => {
                expect(fetch.mock.calls[0][0]).toStrictEqual(
                    'http://localhost:8000/count?disease=cancer&year=2010');
            });

            it('should be called with correct url second time', () => {
                expect(fetch.mock.calls[1][0]).toStrictEqual(
                    'http://localhost:8000/count?disease=cancer&year=2011');
            });
        });

        describe('setChartOptions', () => {
            it('should be called once', () => {
                expect(setChartOptions).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct options', () => {
                expect(setChartOptions.mock.calls[0][0]).toStrictEqual({
                    "dataLabels": {
                        "enabled": false
                    },
                    "xaxis": {
                        "categories": [
                            2010,
                            2011
                        ]
                    }
                });
            });
        });

        describe('setChartSeries', () => {
            it('should be called twice', () => {
                expect(setChartSeries).toHaveBeenCalledTimes(2);
            });

            it('should be called with correct initial series', () => {
                expect(setChartSeries.mock.calls[0][0]).toStrictEqual([ initialChartSeries ]);
            });

            it('should be called with correct final series ', () => {
                expect(setChartSeries.mock.calls[1][0]).toStrictEqual([{
                    name: 'Articles published',
                    data: [5, 5]
                }]);
            });
        });

        describe('setProgress', () => {
            it('should be called three times', () => {
                expect(setProgress).toHaveBeenCalledTimes(3);
            });

            it('should be called with value of zero', () => {
                expect(setProgress.mock.calls[0][0]).toBe(0);
            });

            it('should be called with value of fifty', () => {
                expect(setProgress.mock.calls[1][0]).toBe(50);
            });

            it('should be called with value of one hundred', () => {
                expect(setProgress.mock.calls[2][0]).toBe(100);
            });
        });

        describe('setLoading', () => {
            it('should be called twice', () => {
                expect(setLoading).toHaveBeenCalledTimes(2);
            });

            it('should be called with value of true', () => {
                expect(setLoading.mock.calls[0][0]).toBe(true);
            });

            it('should be called with value of false', () => {
                expect(setLoading.mock.calls[1][0]).toBe(false);
            });
        });
    });
});