import React, { useState } from 'react';
import Inputs from '../inputs';
import Output from '../output';
import { initialChartOptions, initialChartSeries, drawChart } from './helper.js';

export default function App() {
    const [ disease, setDisease ] = useState('');
    const [ startYear, setStartYear ] = useState(1970);
    const [ endYear, setEndYear ] = useState(2021);
    const [ chartOptions, setChartOptions ] = useState(initialChartOptions);
    const [ chartSeries, setChartSeries ] = useState([ initialChartSeries ]);
    const [ loading, setLoading ] = useState(false);
    const [ progress, setProgress] = useState(0);

    return (
        <>
            <Inputs
                disease={disease}
                setDisease={setDisease}
                startYear={startYear}
                setStartYear={setStartYear}
                endYear={endYear}
                setEndYear={setEndYear}
                drawChart={() => drawChart(disease, startYear, endYear, setChartSeries, setChartOptions, setProgress, setLoading)}
                loading={loading}
            />
            <hr className="solid" style={styles.divider}/>
            <Output
                options={chartOptions}
                series={chartSeries}
                loading={loading}
                progress={progress}/>
        </>
    );
}

const styles = {
    divider: {
        borderTop: '1px solid blue',
        marginTop: '12px'
    }
};