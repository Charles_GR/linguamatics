const express = require('express');
const cors = require('cors');
const https = require('https');
const parser = require('xml2js');

const app = express().use(cors());
const port = 8000;

app.get('/', (req, res) => {
    res.sendStatus(200);
});

app.get('/count', (req, res) => {
    const options = {
        hostname: 'eutils.ncbi.nlm.nih.gov',
        path: `/entrez/eutils/esearch.fcgi?db=pubmed&term=${req.query.disease}+AND+${req.query.year}&datetype=pdat&retmax=100000`
    }

    https.get(options, xmlRes => {
      let xml = '';

      xmlRes.on('data', chunk => {
        xml += chunk.toString();
      });

      xmlRes.on('end', () => {
        parser.parseString(xml, function (error, result) {
          res.send({ 'count': parseInt(result.eSearchResult.Count[0]) });
        });
      });
    });
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
});