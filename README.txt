1. Ensure that NodeJS is installed.
2. Open a terminal window here
3. Enter 'cd backend'
4. Enter 'npm install'
5. Enter 'npm start'
6. Open another terminal window here
7. Enter 'cd frontend'
8. Enter 'npm install'
9. Enter 'npm start'
10. Open 'http://localhost:3000' in browser